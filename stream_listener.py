import array
import binascii
import socket
import struct
import time
from threading import Thread
from pydispatch import dispatcher


class GetStream(Thread):
    """The PlayStream thread"""

    def __init__(self, multicast_group, multicast_port=50003):
        self.shutdown = False
        self.multicast_group = multicast_group
        self.multicast_port = int(multicast_port)
        self.stream_num = None
        Thread.__init__(self)

    def run(self):
        server_address = ('', self.multicast_port)
        # Create the socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # Bind to the server address
        sock.bind(server_address)
        # Join
        group = socket.inet_aton(self.multicast_group)
        mreq = struct.pack('4sL', group, socket.INADDR_ANY)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        sock.setblocking(0)
        old_sequence = None
        self.dropped_total = 0
        self.received = 0
        while not self.shutdown:
            try:
                data, address = sock.recvfrom(1080)
                self.stream_num = str(ord(data[6]) * 256 + ord(data[10]))
                dispatcher.send(signal='Debug', sender=self, data=data[:14])
                sequence = int(data[9].encode('hex'), 16)
                if old_sequence is None:
                    old_sequence = sequence
                # print '.',
                if (old_sequence + 1) != sequence:
                    drop = abs(sequence - old_sequence)
                    self.dropped_total += drop
                    # print '#',
                if sequence != 255:
                    old_sequence = sequence
                else:
                    old_sequence = -1
                audio = data[14:]
                y = array.array('h', audio)
                y.byteswap()
                frame = binascii.hexlify(y).decode('hex')
                self.received += 1
                dispatcher.send(signal="Audio", sender=dispatcher.Any, data=frame)
            except Exception as error:
                if error[0] == 10035:
                    # no stream detected
                    # lets wait a bit and check again
                    time.sleep(.1)
                    pass
                else:
                    print "Error getting stream: ", error
        # print 'dropping: ', repr(mreq)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_DROP_MEMBERSHIP, mreq)
        sock.close()
        self.stream_num = ''


def main():

    test = GetStream('239.255.165.12')
    test.setDaemon(True)
    test.start()
    test.shutdown = True
    test.join()


if __name__ == '__main__':
    main()
