import pyaudio
from threading import Thread
from pydispatch import dispatcher


class PlayStream(Thread):
    """The PlayStream thread"""

    def __init__(self):
        self.shutdown = False
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(format=8,
                                  channels=2,
                                  rate=48000,
                                  output=True)
        Thread.__init__(self)

    def run(self):
        dispatcher.connect(self.play, signal="Audio", sender=dispatcher.Any)
        while not self.shutdown:
            pass
        self.stream.stop_stream()
        self.stream.close()
        self.p.terminate()

    def play(self, sender, data):
        """Play the received frame"""
        if not self.shutdown:
            self.stream.write(data)


def main():
    test = PlayStream()
    test.setDaemon(True)
    test.start()
    test.shutdown = True
    test.join()


if __name__ == '__main__':
    main()
