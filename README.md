## Overview 

Stream Receiver is an open source python program used to verify audio streams from SVSi encoders.

## How to Run

	git clone https://bitbucket.org/avt-dev/stream-receiver.git
	cd stream-receiver
	pip install -r requirements
    pip install -U wxPython
	python streamy.py


## To build a Windows executable

	pip install pyinstaller
	edit streamy.spec with your path
	pyinstaller streamy.spec

Or download a pre-compiled copy in the [Downloads](https://bitbucket.org/avt-dev/stream-receiver/downloads/) section

## Unsupported

This software is totally unsupported. If it breaks something you get to keep the pieces.
