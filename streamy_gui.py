# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class StreamyFrame
###########################################################################

class StreamyFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.Size( 310,100 ), wx.DefaultSize )
		
		bSizer1 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer15 = wx.BoxSizer( wx.VERTICAL )
		
		self.stream_sizer = wx.StaticBoxSizer( wx.StaticBox( self.m_panel1, wx.ID_ANY, u"Stream ID" ), wx.HORIZONTAL )
		
		
		bSizer15.Add( self.stream_sizer, 0, wx.ALL|wx.EXPAND, 5 )
		
		bSizer7 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.rx_group_label = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Address", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.rx_group_label.Wrap( -1 )
		self.rx_group_label.SetMinSize( wx.Size( 50,-1 ) )
		
		bSizer7.Add( self.rx_group_label, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.rx_group_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer7.Add( self.rx_group_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer15.Add( bSizer7, 0, wx.EXPAND|wx.ALL, 5 )
		
		bSizer91 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.rx_id_label = wx.StaticText( self.m_panel1, wx.ID_ANY, u"ID", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.rx_id_label.Wrap( -1 )
		self.rx_id_label.SetMinSize( wx.Size( 50,-1 ) )
		
		bSizer91.Add( self.rx_id_label, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.rx_id_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer91.Add( self.rx_id_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer15.Add( bSizer91, 0, wx.EXPAND|wx.ALL, 5 )
		
		bSizer9 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.rx_rcvd_label = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Received", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.rx_rcvd_label.Wrap( -1 )
		self.rx_rcvd_label.SetMinSize( wx.Size( 50,-1 ) )
		
		bSizer9.Add( self.rx_rcvd_label, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.rx_rcvd_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer9.Add( self.rx_rcvd_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer15.Add( bSizer9, 0, wx.EXPAND|wx.ALL, 5 )
		
		bSizer10 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.rx_dropped_label = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Dropped", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.rx_dropped_label.Wrap( -1 )
		self.rx_dropped_label.SetMinSize( wx.Size( 50,-1 ) )
		
		bSizer10.Add( self.rx_dropped_label, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.rx_dropped_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer10.Add( self.rx_dropped_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer15.Add( bSizer10, 0, wx.EXPAND|wx.ALL, 5 )
		
		
		bSizer2.Add( bSizer15, 1, wx.EXPAND, 5 )
		
		
		self.m_panel1.SetSizer( bSizer2 )
		self.m_panel1.Layout()
		bSizer2.Fit( self.m_panel1 )
		bSizer1.Add( self.m_panel1, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer1 )
		self.Layout()
		bSizer1.Fit( self )
		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.m_menuItem31 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Toggle Details", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.AppendItem( self.m_menuItem31 )
		
		self.rx_interleave_chk = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Interleave Streams", wx.EmptyString, wx.ITEM_CHECK )
		self.m_menu1.AppendItem( self.rx_interleave_chk )
		
		self.check_for_updates_chk = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Check for Updates", wx.EmptyString, wx.ITEM_CHECK )
		self.m_menu1.AppendItem( self.check_for_updates_chk )
		self.check_for_updates_chk.Check( True )
		
		self.m_menuItem2 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.AppendItem( self.m_menuItem2 )
		
		self.m_menubar1.Append( self.m_menu1, u"File" ) 
		
		self.m_menu2 = wx.Menu()
		self.m_menuItem5 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Help", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.AppendItem( self.m_menuItem5 )
		
		self.m_menubar1.Append( self.m_menu2, u"About" ) 
		
		self.SetMenuBar( self.m_menubar1 )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.on_close )
		self.Bind( wx.EVT_MENU, self.on_toggle_details, id = self.m_menuItem31.GetId() )
		self.Bind( wx.EVT_MENU, self.on_toggle_interleave, id = self.rx_interleave_chk.GetId() )
		self.Bind( wx.EVT_MENU, self.on_toggle_updates, id = self.check_for_updates_chk.GetId() )
		self.Bind( wx.EVT_MENU, self.on_close, id = self.m_menuItem2.GetId() )
		self.Bind( wx.EVT_MENU, self.on_documentation, id = self.m_menuItem5.GetId() )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def on_close( self, event ):
		event.Skip()
	
	def on_toggle_details( self, event ):
		event.Skip()
	
	def on_toggle_interleave( self, event ):
		event.Skip()
	
	def on_toggle_updates( self, event ):
		event.Skip()
	
	
	def on_documentation( self, event ):
		event.Skip()
	

###########################################################################
## Class AboutFrame
###########################################################################

class AboutFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 800,700 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

