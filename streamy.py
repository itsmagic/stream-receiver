import wx
import wx.html
import os
import json
import streamy_gui
import stream_listener
import stream_player
import ipaddress
import sys
import webbrowser
from bitbucket_updater import bitbucket_updater
from pydispatch import dispatcher

# The MIT License (MIT)

# Copyright (c) 2017 Jim Maciejewski

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


class Streamy_Frame(streamy_gui.StreamyFrame):
    def __init__(self, parent):
        streamy_gui.StreamyFrame.__init__(self, parent)

        icon_bundle = wx.IconBundle()
        icon_bundle.AddIconFromFile(self.resource_path(os.path.join("icon", "st.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcons(icon_bundle)
        self.name = "Stream Receiver"
        self.version = "v0.0.2"
        self.SetTitle(self.name + " " + self.version)
        self.listener = None
        self.playing = False
        self.details_hidden = False
        self.hide_details()
        self.custom_streams = {}
        self.load_config()
        self.load_available_streams()
        self.old_count = 0
        self.redraw_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_refresh, self.redraw_timer)
        self.redraw_timer.Start(1000)
        self.Fit()
        self.check_for_updates()

    def resource_path(self, relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)

    def on_refresh(self, event):
        """Update the stream number"""
        try:
            self.rx_id_txt.SetLabel(self.listener.stream_num)
        except AttributeError:
            self.rx_id_txt.SetLabel('')
        except TypeError:
            self.rx_id_txt.SetLabel('Waiting for stream')
        except Exception as error:
            print "on refresh: ", repr(error)
        try:
            self.rx_dropped_txt.SetLabel(str(self.listener.dropped_total))
            self.rx_rcvd_txt.SetLabel(str(self.listener.received))
            if self.old_count < self.listener.received:
                # looks like we are getting a stream
                self.play_btn.SetBackgroundColour('green')
                self.play_btn.Refresh()
                self.old_count = self.listener.received
            else:
                self.play_btn.SetBackgroundColour(wx.NullColour)
                self.play_btn.Refresh()
                self.old_count = self.listener.received
        except AttributeError:
            self.rx_dropped_txt.SetLabel('')
            self.rx_rcvd_txt.SetLabel('')
            pass
        except Exception as error:
            print "on refresh: ", repr(error)

    def on_rx_select_entry(self, event):
        """Checks if valid stream or group"""
        self.stop_stream_listener()
        stream_number = self.custom_streams[self.rx_txt_input.GetStringSelection()]
        if self.rx_interleave_chk.IsChecked():
            feedback, group, stream = self.check_stream_or_group_interleave(stream_number, 'rx')
        else:
            feedback, group, stream = self.check_stream_or_group(stream_number, 'rx')
        self.stream_feedback(feedback, 'rx')

        if group is not None:
            # print "start stream: ", group
            self.start_stream_listener(group)
            self.save_config()

    def on_rx_text_entry(self, event):
        """Checks if valid stream or group"""
        self.stop_stream_listener()
        self.rx_dropped_txt.SetLabel('')
        self.rx_rcvd_txt.SetLabel('')
        stream_number = self.rx_txt_input.GetValue()
        if self.rx_interleave_chk.IsChecked():
            feedback, group, stream = self.check_stream_or_group_interleave(stream_number, 'rx')
        else:
            feedback, group, stream = self.check_stream_or_group(stream_number, 'rx')
        self.stream_feedback(feedback, 'rx')

        if group is not None:
            # print "start stream: ", group
            self.start_stream_listener(group)
            self.save_config()

    def check_stream_or_group_interleave(self, text, rx_or_tx):
        """Checks if we have a valid stream or group in interleave mode """
        try:
            if "." in text:
                # We have a group address
                group = text
            elif text == "":
                return 'warn', None, None
            elif str(text).isdigit():
                # We have a stream number
                # First we check if it is "invalid"
                if (int(text)) % 128 == 0:
                    # print "modo"
                    return 'invalid', None, None
                group = self.convert_stream_to_multicast_interleave(text)
                # print 'group: ', group
            else:
                # print 'not a digit, not text?'
                return 'invalid', None, None
        except Exception as error:
            print "GetValue error: ", repr(error)
            return 'invalid', None, None

        try:
            if ipaddress.ip_address(group).is_multicast:
                last = group.split('.')[-1]
                if last in ['0', '255']:
                    # print 'last 0 or 255'
                    return 'invalid', None, None
                group_list = group.split('.')
                # print "group: ", group_list[2], group_list[3]
                if (group_list[0] != '239' or
                        group_list[1] != '255' or
                        int(group_list[3]) % 2 or
                        (group_list[2] == "255" and group_list[3] == "254")):
                        # print 'doesnt start with 239.255 and the last octet is odd, and the last two are not "255.254"'
                    return 'invalid', None, None
                stream = str(self.convert_multicast_to_stream_interleave(group))
                return 'valid', group, stream
        except ValueError:
            # not an multicast address
            # print 'not multicast address'
            return 'invalid', None, None
        except Exception as error:
            print "Stream or Group Error: ", repr(error)
            return 'invalid', None, None

    def convert_stream_to_multicast_interleave(self, stream):
        """Converts a stream number to a interleave multicast group"""
        stream = int(stream)
        if stream / 128 == 0:
            a = 0
        else:
            a = stream / 128
        b = (stream % 128) * 2
        group = u"239.255.{}.{}".format(str(a), str(b))
        # print 'convert stream to multicast: ', group
        return group

    def convert_multicast_to_stream_interleave(self, group):
        """Converts a interleave multicast group to a stream number"""

        a, b = group.split('.')[2:]
        a = int(a) * 128
        b = int(b) / 2
        # if int(a) == 165:
        #     a = 0
        # else:
        #     a = int(a) - 128
        #     a = a * 256
        # print "a: ", a, "b: ", b
        return int(a) + int(b)

    def check_stream_or_group(self, text, rx_or_tx):
        """Checks if we have a valid stream or group """
        try:
            if "." in text:
                # We have a group address
                group = text
            elif text == "":
                return 'warn', None, None
            elif str(text).isdigit():
                # We have a stream number
                # First we check if it is "invalid"
                if (int(text) + 1) % 256 == 0 or (int(text)) % 256 == 0 or int(text) == 0:
                    # print "modo"
                    return 'invalid', None, None
                group = self.convert_stream_to_multicast(text)
            else:
                # print 'not a digit, not text?'
                return 'invalid', None, None
        except Exception as error:
            print "GetValue error: ", repr(error)
            return 'invalid', None, None

        try:
            if ipaddress.ip_address(group).is_multicast:
                last = group.split('.')[-1]
                if last in ['0', '255']:
                    # print 'last 0 or 255'
                    return 'invalid', None, None
                group_list = group.split('.')
                if (group_list[0] != '239' or group_list[1] != '255' or int(group_list[2]) < 128):
                    # print "doesn't start with 239.255 or 3 octet less than 128"
                    return 'invalid', None, None
                stream = str(self.convert_multicast_to_stream(group))
                return 'valid', group, stream
        except ValueError:
            # not an multicast address
            # print 'not multicast address'
            return 'invalid', None, None
        except Exception as error:
            print "Stream or Group Error: ", repr(error)
            return 'invalid', None, None

    def convert_stream_to_multicast(self, stream):
        """Converts a stream number to a multicast group"""
        stream = int(stream)
        if stream / 256 == 0:
            a = 165
        elif stream / 256 == 37:
            a = 128
        else:
            a = stream / 256 + 128
        b = stream % 256
        group = u"239.255.{}.{}".format(str(a), str(b))
        # print 'convert stream to multicast: ', group
        return group

    def convert_multicast_to_stream(self, group):
        """Converts a multicast group to a stream number"""

        a, b = group.split('.')[2:]
        if int(a) == 165:
            a = 0
        elif int(a) == 128:
            a = 37 * 256
        else:
            a = int(a) - 128
            a = a * 256
        # print "a: ", a, "b: ", b
        return int(a) + int(b)

    def stream_feedback(self, notification, item):
        """Feed back on invalid stream"""
        if item == 'rx':
            if notification == "warn":
                self.rx_txt_input.SetBackgroundColour('yellow')
                self.rx_txt_input.Refresh()
            if notification == "invalid":
                self.rx_txt_input.SetBackgroundColour((232, 81, 81))
                self.rx_txt_input.Refresh()
            if notification == "valid":
                self.rx_txt_input.SetBackgroundColour(wx.NullColour)
                self.rx_txt_input.Refresh()

        if item == 'tx':
            if notification == "warn":
                self.tx_txt_input.SetBackgroundColour('yellow')
                self.tx_txt_input.Refresh()
            if notification == "invalid":
                self.tx_txt_input.SetBackgroundColour((232, 81, 81))
                self.tx_txt_input.Refresh()
            if notification == "valid":
                self.tx_txt_input.SetBackgroundColour(wx.NullColour)
                self.tx_txt_input.Refresh()

    def start_stream_listener(self, group):
        self.listener = stream_listener.GetStream(group)
        self.listener.setDaemon(True)
        self.listener.start()
        self.rx_group_txt.SetLabel(group + ':50003')
        # self.m_port_txt.SetLabel('50003')
        self.play_btn.Enable(True)

    def stop_stream_listener(self):
        try:
            if hasattr(self.listener, 'shutdown'):
                self.listener.shutdown = True
                self.listener.join()
                del self.listener
                self.rx_group_txt.SetLabel('')
                # self.m_port_txt.SetLabel('')
                self.play_btn.Enable(False)

        except AttributeError:
            pass
        except Exception as error:
            print "Stop stream listener error: ", repr(error)

    def on_play_toggle(self, event):
        """Either play or stop """
        if self.playing:
            self.play_stop()
        else:
            self.play_start()

    def play_start(self):
        """Play the stream"""
        self.player = stream_player.PlayStream()
        self.player.setDaemon(True)
        self.player.start()
        self.play_btn.SetLabel('Stop')
        self.playing = True

    def play_stop(self):
        """Stop the stream"""
        self.player.shutdown = True
        self.player.join()
        self.play_btn.SetLabel('Play')
        self.playing = False

    def on_close(self, event):
        """Close all threads"""
        try:
            self.listener.shutdown = True
            self.listener.join()
        except AttributeError:
            pass
        try:
            self.player.shutdown = True
            self.player.join()
        except AttributeError:
            pass
        self.Destroy()

    def load_available_streams(self):
        """Loads the customer streams"""
        if self.custom_streams:
            self.rx_txt_input = wx.Choice(self.stream_sizer.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, self.custom_streams.keys(), 0)
            self.rx_txt_input.SetSelection(0)
            self.stream_sizer.Add(self.rx_txt_input, 1, wx.ALL | wx.EXPAND, 5)
            self.rx_txt_input.Bind(wx.EVT_CHOICE, self.on_rx_select_entry)
        else:
            # no streams given, give them a entry
            self.rx_txt_input = wx.TextCtrl(self.stream_sizer.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
            self.rx_txt_input.SetToolTipString(u"Please enter the Stream ID or Multicast Group.\nA red backgroud indicates invalid values.")
            self.stream_sizer.Add(self.rx_txt_input, 0, wx.ALL | wx.EXPAND, 5)
            self.rx_txt_input.Bind(wx.EVT_TEXT, self.on_rx_text_entry)
        self.play_btn = wx.Button(self.stream_sizer.GetStaticBox(), wx.ID_ANY, u"Play", wx.DefaultPosition, wx.DefaultSize, 0)
        self.stream_sizer.Add(self.play_btn, 0, wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
        self.play_btn.Bind(wx.EVT_BUTTON, self.on_play_toggle)
        if len(self.custom_streams) != 0:
            self.on_rx_select_entry(None)
        else:
            self.rx_txt_input.SetValue(self.rx_config_stream)

    def load_config(self):
        """Loads the saved config"""
        try:
            with open('stream_receiver.conf', 'rb') as f:
                config = json.load(f)
            if "rx_stream" in config:
                if config['rx_stream'] is not None:
                    self.rx_config_stream = config['rx_stream']
                else:
                    self.rx_config_stream = '1'
            if "custom_streams" in config:
                self.custom_streams = config['custom_streams']
            if "interleave" in config:
                self.rx_interleave_chk.Check(config['interleave'])
            if "check_for_updates" in config:
                self.check_for_updates_chk.Check(config['check_for_updates'])

        except Exception as error:
            self.rx_config_stream = '1'
            print "unable to load configuration: ", error

    def save_config(self):
        """Save the ip addresses for next time"""
        try:
            rx_stream = self.rx_txt_input.GetValue()
        except AttributeError:
            rx_stream = None
        try:
            with open('stream_receiver.conf', 'wb') as f:
                config = {"rx_stream": rx_stream,
                          "custom_streams": self.custom_streams,
                          "interleave": self.rx_interleave_chk.IsChecked(),
                          "check_for_updates": self.check_for_updates_chk.IsChecked()
                          }
                json.dump(config, f)
        except Exception as error:
            print "unable to save configuration: ", repr(error)

    def on_toggle_details(self, event):
        """Toggles the details"""
        if self.details_hidden:
            self.show_details()
        else:
            self.hide_details()

    def hide_details(self):
        self.rx_group_label.Hide()
        self.rx_group_txt.Hide()
        self.rx_id_label.Hide()
        self.rx_id_txt.Hide()
        self.rx_rcvd_label.Hide()
        self.rx_rcvd_txt.Hide()
        self.rx_dropped_label.Hide()
        self.rx_dropped_txt.Hide()
        self.details_hidden = True
        self.Fit()

    def show_details(self):
        self.rx_group_label.Show()
        self.rx_group_txt.Show()
        self.rx_id_label.Show()
        self.rx_id_txt.Show()
        self.rx_rcvd_label.Show()
        self.rx_rcvd_txt.Show()
        self.rx_dropped_label.Show()
        self.rx_dropped_txt.Show()
        self.details_hidden = False
        self.Fit()

    def on_documentation(self, event):
        aboutDlg = streamy_gui.AboutFrame(None)
        html = wx.html.HtmlWindow(aboutDlg)
        with open(self.resource_path(os.path.join('doc', 'help_doc.html')), 'rb') as f:
            html.SetPage(f.read())
        aboutDlg.Show()

    def on_toggle_interleave(self, event):
        """Toggles the check on interleave"""
        if self.rx_interleave_chk.IsChecked():
            self.rx_interleave_chk.Check(True)
        else:
            self.rx_interleave_chk.Check(False)
        if self.custom_streams:
            self.on_rx_select_entry(None)
        else:
            self.on_rx_text_entry(None)
        self.save_config()

    def on_toggle_updates(self, event):
        """Toggle the check for updates menu"""
        if self.check_for_updates_chk.IsChecked():
            self.check_for_updates_chk.Check(True)
            self.check_for_updates()
        else:
            self.check_for_updates_chk.Check(False)
        self.save_config()

    def check_for_updates(self):
        """Check for updates"""
        if not self.check_for_updates_chk.IsChecked():
            return
        repo_user = 'avt-dev'
        repo_name = 'stream-receiver'
        dispatcher.connect(self.incoming_updates, signal="Bitbucket Updater", sender=dispatcher.Any)
        self.updater = bitbucket_updater.Updater(version=self.version, repo_user=repo_user, repo_name=repo_name)
        self.updater.setDaemon(True)
        self.updater.start()

    def incoming_updates(self, sender, data):
        """Handle updates"""
        if 'update avaliable' in data:
            message = 'An update for {} is available. \rClick ok to download.\r\r {}\r'.format(self.name, data['name'])
            dlg = wx.MessageDialog(parent=self,
                                   message=message,
                                   caption='Do you want to update?',
                                   style=wx.OK | wx.CANCEL)
            if dlg.ShowModal() != wx.ID_OK:
                dlg.Destroy()
                return
            dlg.Destroy()
            webbrowser.open(data['url'])


def main():
    """run the main program"""
    st_app = wx.App()  # redirect=True, filename="log.txt")
    # do processing/initialization here and create main window
    st_frame = Streamy_Frame(None)
    st_frame.Show()
    st_app.MainLoop()


if __name__ == '__main__':
    main()
